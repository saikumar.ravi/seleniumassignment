package com.SeleniumAssignment;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

public class OrangeHRMAss1 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		WebDriver driver=new ChromeDriver();
		driver.manage().window().maximize();
		driver.get("https://opensource-demo.orangehrmlive.com/");
		driver.manage().timeouts().implicitlyWait(10,TimeUnit.SECONDS);
		
		
		driver.findElement(By.xpath("//input[@name='username']")).sendKeys("Admin");
		
		driver.findElement(By.xpath("//input[@name='password']")).sendKeys("admin123");
		driver.findElement(By.xpath("//button[@class='oxd-button oxd-button--medium oxd-button--main orangehrm-login-button']")).click();
		
		driver.findElement(By.xpath("//a[@class='oxd-main-menu-item'][1]")).click();
		
		driver.findElement(By.xpath("(//button[@type='button'])[4]")).click();
				
		driver.findElement(By.xpath("//span[text()='Organization ']")).click();
		driver.findElement(By.partialLinkText("Locations")).click();

		driver.findElement(By.xpath("//button[text()=' Add ']")).click();
		driver.findElement(By.xpath("(//input[@placeholder='Type here ...'])[1]")).sendKeys("QA");
		driver.findElement(By.xpath("(//input[@placeholder='Type here ...'])[2]")).sendKeys("Banglore");
		driver.findElement(By.xpath("(//input[@placeholder='Type here ...'])[3]")).sendKeys("Karnataka");
		driver.findElement(By.xpath("(//input[@placeholder='Type here ...'])[4]")).sendKeys("560002");
		driver.findElement(By.xpath("//i[@class='oxd-icon bi-caret-down-fill oxd-select-text--arrow']")).click();
		driver.findElement(By.xpath("//*[contains(text(),'United States')]")).click();

		driver.findElement(By.xpath("(//input[@placeholder='Type here ...'])[5]")).sendKeys("956667345");
		driver.findElement(By.xpath("(//input[@placeholder='Type here ...'])[6]")).sendKeys("686764");
		driver.findElement(By.xpath("(//textarea[@placeholder='Type here ...'])[1]"))
				.sendKeys("23 new helton street chennai tamilnadu");
		driver.findElement(By.xpath("(//textarea[@placeholder='Type here ...'])[2]")).sendKeys("hello world");
		driver.findElement(By.xpath("//button[@type='submit']")).click();
		String text = driver.findElement(By.xpath("//div[text()='Automation engineere']")).getText();
		System.out.println(text);
		driver.findElement(By.xpath("//i[@class=\"oxd-icon bi-caret-down-fill oxd-userdropdown-icon\"]")).click();
		driver.findElement(By.xpath("//a[text()='Logout']")).click();
		driver.close();
	}

}
