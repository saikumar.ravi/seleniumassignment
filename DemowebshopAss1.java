package com.SeleniumAssignment;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.Select;

public class DemowebshopAss1 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		WebDriver driver = new ChromeDriver();
		driver.get("https://demowebshop.tricentis.com/login");
		driver.findElement(By.id("Email")).sendKeys("ravisaikumar97@gmail.com");
		driver.findElement(By.id("Password")).sendKeys("Sai@1234");
		driver.findElement(By.xpath("//input[@value='Log in']")).click();
		WebElement Computer= driver.findElement(By.xpath("//ul[@class='top-menu']//a[contains(text(),'Computers')]"));
		Actions act = new Actions(driver);
		act.moveToElement(Computer).build().perform();
		act.moveToElement(driver.findElement(By.partialLinkText("Desktops"))).click().perform();
		WebElement element=driver.findElement(By.id("products-orderby"));
		Select s = new Select(element);
		s.selectByVisibleText("Price: Low to High");
		driver.findElement(By.xpath("(//input[@value='Add to cart'])[1]")).click();
		driver.findElement(By.id("add-to-cart-button-72")).click();
		driver.findElement(By.linkText("Shopping cart")).click();
		driver.findElement(By.id("termsofservice")).click();
		driver.findElement(By.id("checkout")).click();
		WebElement selectcountry= driver.findElement(By.id("BillingNewAddress_CountryId"));
		Select sc1= new Select(selectcountry);
		sc1.selectByVisibleText("India");
		driver.findElement(By.id("BillingNewAddress_City")).sendKeys("chennai");
		driver.findElement(By.id("BillingNewAddress_Address1")).sendKeys("rowyapuram");
		driver.findElement(By.id("BillingNewAddress_ZipPostalCode")).sendKeys("600021");
		driver.findElement(By.id("BillingNewAddress_PhoneNumber")).sendKeys("98765677834");
		driver.findElement(By.xpath("(//input[@title='Continue'])[1]")).click();
		driver.findElement(By.id("PickUpInStore")).click();
		driver.findElement(By.xpath("//input[@onclick='Shipping.save()']")).click();
		driver.findElement(By.xpath("//input[@onclick='PaymentMethod.save()']")).click();
		driver.findElement(By.xpath("//input[@onclick='PaymentInfo.save()']")).click();
		JavascriptExecutor js= (JavascriptExecutor)driver;
		js.executeScript("document.getElementById('confirm-order-buttons-container').scrollIntoView()");
		driver.findElement(By.xpath("//input[@onclick='ConfirmOrder.save()']")).click();
		WebElement text= driver.findElement(By.xpath("//div[@class='section order-completed']"));
		System.out.println(text.getText());
        driver.close();


	}

}
