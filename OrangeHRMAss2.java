package com.SeleniumAssignment;

import java.awt.AWTException;
import java.awt.Robot;
import java.awt.event.KeyEvent;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

public class OrangeHRMAss2 {

	public static void main(String[] args) throws AWTException {
		// TODO Auto-generated method stub
		WebDriver driver = new ChromeDriver();
		driver.manage().window().maximize();
		driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
		driver.get("https://opensource-demo.orangehrmlive.com/web/index.php/auth/login");
		driver.findElement(By.name("username")).sendKeys("Admin");

		driver.findElement(By.name("password")).sendKeys("admin123");
		driver.findElement(By.xpath("//button[text()=' Login ']")).click();
		driver.findElement(By.xpath("//span[text()='Admin']")).click();
		driver.findElement(By.xpath("//span[text()='User Management ']")).click();
		driver.findElement(By.linkText("Users")).click();
		driver.findElement(By.xpath("//button[text()=' Add ']")).click();
		driver.findElement(By.xpath("(//i[@class='oxd-icon bi-caret-down-fill oxd-select-text--arrow'])[1]")).click();
		driver.findElement(By.xpath("//*[contains(text(),'Admin')]")).click();
		Robot r = new Robot();
		WebElement ele = driver.findElement(By.xpath("//input[@placeholder=\"Type for hints...\"]"));
		ele.sendKeys("paul");
		driver.findElement(By.xpath("//*[contains(text(),'Paul  Collings')]")).click();
		driver.findElement(By.xpath("(//i[@class='oxd-icon bi-caret-down-fill oxd-select-text--arrow'])[2]")).click();
		driver.findElement(By.xpath("//*[contains(text(),'Enabled')]")).click();
		r.keyPress(KeyEvent.VK_DOWN);
		r.keyRelease(KeyEvent.VK_DOWN);
		r.keyPress(KeyEvent.VK_ENTER);
		r.keyRelease(KeyEvent.VK_ENTER);
		driver.findElement(By.xpath("(//input[@class='oxd-input oxd-input--active'])[2]")).sendKeys(" sessions");
		driver.findElement(By.xpath("(//input[@type=\"password\"])[1]")).sendKeys("Sai@1234");
		driver.findElement(By.xpath("(//input[@type=\"password\"])[2]")).sendKeys("Sai@1234");
		driver.findElement(By.xpath("//button[text()=' Save ']")).click();
		String text = driver.findElement(By.xpath("//div[text()='sessions']")).getText();
		System.out.println(text);
		driver.findElement(By.xpath("//i[@class=\"oxd-icon bi-caret-down-fill oxd-userdropdown-icon\"]")).click();
		driver.findElement(By.xpath("//a[text()='Logout']")).click();
		driver.close();

	}

}
